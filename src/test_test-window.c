/* test_test-window.c
 *
 * Copyright 2022 Ignacy Kuchciński
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_test-config.h"
#include "test_test-window.h"

#include "pages/more/test_test-page-more.h"
#include "pages/dino/test_test-page-dino.h"
#include "pages/sintel/test_test-page-sintel.h"
#include "pages/recs/test_test-page-recs.h"
#include "pages/tuner/test_test-page-tuner.h"
#include "pages/gsoc/test_test-page-gsoc.h"

struct _TestTestWindow
{
  AdwApplicationWindow  parent_instance;

  /* Template widgets */
  AdwLeaflet *leaflet;
};

G_DEFINE_TYPE (TestTestWindow, test_test_window, ADW_TYPE_APPLICATION_WINDOW)

static void
back_clicked_cb (TestTestWindow *self)
{
  adw_leaflet_navigate (self->leaflet, ADW_NAVIGATION_DIRECTION_BACK);
}

static void
notify_visible_child_cb (TestTestWindow *self)
{
  adw_leaflet_navigate (self->leaflet, ADW_NAVIGATION_DIRECTION_FORWARD);
}

static void
test_test_window_class_init (TestTestWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/testtest/test_test-window.ui");
  gtk_widget_class_bind_template_child (widget_class, TestTestWindow, leaflet);
  gtk_widget_class_bind_template_callback (widget_class, back_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, notify_visible_child_cb);
}

static void
test_test_window_init (TestTestWindow *self)
{
  g_type_ensure (TEST_TEST_TYPE_PAGE_MORE);
  g_type_ensure (TEST_TEST_TYPE_PAGE_DINO);
  g_type_ensure (TEST_TEST_TYPE_PAGE_SINTEL);
  g_type_ensure (TEST_TEST_TYPE_PAGE_RECS);
  g_type_ensure (TEST_TEST_TYPE_PAGE_TUNER);
  g_type_ensure (TEST_TEST_TYPE_PAGE_GSOC);
  gtk_widget_init_template (GTK_WIDGET (self));
  adw_leaflet_navigate (self->leaflet, ADW_NAVIGATION_DIRECTION_FORWARD);
}
