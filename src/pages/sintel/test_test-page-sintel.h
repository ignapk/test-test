#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define TEST_TEST_TYPE_PAGE_SINTEL (test_test_page_sintel_get_type())

G_DECLARE_FINAL_TYPE (TestTestPageSintel, test_test_page_sintel, TEST_TEST, PAGE_SINTEL, AdwBin);

G_END_DECLS
