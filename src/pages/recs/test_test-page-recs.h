#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define TEST_TEST_TYPE_PAGE_RECS (test_test_page_recs_get_type())

G_DECLARE_FINAL_TYPE (TestTestPageRecs, test_test_page_recs, TEST_TEST, PAGE_RECS, AdwBin);

G_END_DECLS
