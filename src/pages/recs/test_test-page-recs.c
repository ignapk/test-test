#include "test_test-page-recs.h"
#include <gst/gst.h>

struct _TestTestPageRecs
{
  AdwBin parent_instance;

  GtkButton *play_pause_button;
  GstElement *pipeline;
  int started;
};

G_DEFINE_TYPE (TestTestPageRecs, test_test_page_recs, ADW_TYPE_BIN);

static void
record_clicked_cb (TestTestPageRecs *self)
{

  if (self->started)
    {
      self->started = 0;
      gtk_button_set_icon_name (self->play_pause_button, "media-record-symbolic");
      gst_element_set_state (self->pipeline, GST_STATE_NULL);
    }
  else
    {
      self->started = 1;
      gtk_button_set_icon_name (self->play_pause_button, "media-playback-stop-symbolic");
      gst_element_set_state (self->pipeline, GST_STATE_PLAYING);
    }
}

static void
test_test_page_recs_finalize (GObject *object)
{
  TestTestPageRecs *self = TEST_TEST_PAGE_RECS (object);

  gst_object_unref (self->pipeline);

  G_OBJECT_CLASS (test_test_page_recs_parent_class)->finalize (object);
}

static void
test_test_page_recs_class_init (TestTestPageRecsClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = test_test_page_recs_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/testtest/pages/recs/test_test-page-recs.ui");
  gtk_widget_class_bind_template_child (widget_class, TestTestPageRecs, play_pause_button);
  gtk_widget_class_bind_template_callback (widget_class, record_clicked_cb);
}

static void
test_test_page_recs_init (TestTestPageRecs *self)
{
  GstElement *source, *wavenc, *filesink;

  gtk_widget_init_template (GTK_WIDGET (self));

  self->started = 0;

  source = gst_element_factory_make ("pulsesrc", "source");
  wavenc = gst_element_factory_make ("wavenc", "wavenc");
  filesink = gst_element_factory_make ("filesink", "filesink");

  self->pipeline = gst_pipeline_new ("pipe");

  gst_bin_add_many (GST_BIN (self->pipeline), source, wavenc, filesink, NULL);
  gst_element_link (source, wavenc);

  g_object_set (filesink, "location", "my_recording", NULL);

  gst_element_link (wavenc, filesink);
}
