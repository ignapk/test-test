#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define TEST_TEST_TYPE_PAGE_GSOC (test_test_page_gsoc_get_type())

G_DECLARE_FINAL_TYPE (TestTestPageGsoc, test_test_page_gsoc, TEST_TEST, PAGE_GSOC, AdwBin)

G_END_DECLS
