#include "test_test-page-gsoc.h"

struct _TestTestPageGsoc
{
  AdwBin parent_instance;
};

G_DEFINE_TYPE (TestTestPageGsoc, test_test_page_gsoc, ADW_TYPE_BIN)

static void
on_import_response (GtkNativeDialog  *native G_GNUC_UNUSED,
                    int               response,
                    TestTestPageGsoc *self G_GNUC_UNUSED)
{
  // If the user selected a file...
  if (response == GTK_RESPONSE_ACCEPT)
    {
      GtkFileChooser *chooser = GTK_FILE_CHOOSER (native);

      // ... retrieve the location from the dialog...
      g_autoptr (GFile) file = gtk_file_chooser_get_file (chooser);

      // ... and copy it to the XDG Templates directory, usually ~/Templates
      g_autofree char *path = NULL;
      path = g_file_get_path (file);
      g_print ("Copying file: %s", path);
      // todo: consider executing a function that does async operation,
      // just like in the gnome developer documentation
    }

  // Release the reference on the file selection dialog now that we
  // do not need it anymore
  g_object_unref (native);
}

static void
test_test_page_gsoc_import_template_dialog (TestTestPageGsoc *self G_GNUC_UNUSED)
{
  // Create a new file selection dialog, using the "open" mode
  GtkFileChooserNative *native = gtk_file_chooser_native_new ("Import Template",
                                                              GTK_WINDOW (self),
                                                              GTK_FILE_CHOOSER_ACTION_OPEN,
                                                              "_Import",
                                                              "_Cancel");

  // Connect the "response" signal of the file selection dialog;
  // this signal is emitted when the user selects a file, or when
  // they cancel the operation
  g_signal_connect (native,
                    "response",
                    G_CALLBACK (on_import_response),
                    self);

  // Present the dialog to the user
  gtk_native_dialog_show (GTK_NATIVE_DIALOG (native));
}



static void
test_test_page_gsoc_class_init (TestTestPageGsocClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/testtest/pages/gsoc/test_test-page-gsoc.ui");
  gtk_widget_class_bind_template_callback (widget_class, test_test_page_gsoc_import_template_dialog);
}

static void
test_test_page_gsoc_init (TestTestPageGsoc *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
