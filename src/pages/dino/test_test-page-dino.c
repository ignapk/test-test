#include "test_test-page-dino.h"

struct _TestTestPageDino
{
  AdwBin parent_instance;

  GtkLabel            *my_label;
  GtkPicture          *my_picture;
};

G_DEFINE_TYPE (TestTestPageDino, test_test_page_dino, ADW_TYPE_BIN)

static void
on_feed_button_clicked_cb (GtkButton        *button G_GNUC_UNUSED,
                           TestTestPageDino *self)
{
  gtk_label_set_text (self->my_label, "Soo tasty!");
}

static void
on_pet_button_clicked_cb (GtkButton        *button G_GNUC_UNUSED,
                          TestTestPageDino *self)
{
  gtk_label_set_text (self->my_label, "*happy noises*");
}

static void
on_good_button_clicked_cb (GtkButton        *button G_GNUC_UNUSED,
                           TestTestPageDino *self)
{
  gtk_picture_set_resource (self->my_picture, "/org/example/testtest/dino.png");
}

static void
on_bad_button_clicked_cb (GtkButton        *button G_GNUC_UNUSED,
                          TestTestPageDino *self)
{
  gtk_picture_set_resource (self->my_picture, "/org/example/testtest/angry_dino.jpg");
}

static void
test_test_page_dino_class_init (TestTestPageDinoClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/testtest/pages/dino/test_test-page-dino.ui");
  gtk_widget_class_bind_template_child (widget_class, TestTestPageDino, my_label);
  gtk_widget_class_bind_template_child (widget_class, TestTestPageDino, my_picture);
  gtk_widget_class_bind_template_callback (widget_class, on_feed_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_pet_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_good_button_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, on_bad_button_clicked_cb);
}

static void
test_test_page_dino_init (TestTestPageDino *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
