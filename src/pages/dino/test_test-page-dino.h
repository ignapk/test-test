#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define TEST_TEST_TYPE_PAGE_DINO (test_test_page_dino_get_type())

G_DECLARE_FINAL_TYPE (TestTestPageDino, test_test_page_dino, TEST_TEST, PAGE_DINO, AdwBin)

G_END_DECLS
