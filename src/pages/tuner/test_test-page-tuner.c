#include "test_test-page-tuner.h"
#include <gst/gst.h>

static guint spect_bands = 100;

#define AUDIOFREQ 32000

struct _TestTestPageTuner
{
  AdwBin parent_instance;

  GstElement *pipeline;


};

G_DEFINE_TYPE (TestTestPageTuner, test_test_page_tuner, ADW_TYPE_BIN);

static gboolean
message_handler (GstBus     *bus G_GNUC_UNUSED,
                 GstMessage *message,
                 gpointer    data)
{
  TestTestPageTuner *self = TEST_TEST_PAGE_TUNER (data);

  if (message->type == GST_MESSAGE_ELEMENT)
    {
      const GstStructure *s = gst_message_get_structure (message);
      const gchar *name = gst_structure_get_name (s);
      GstClockTime endtime;

      if (strcmp (name, "spectrum") == 0)
        {
          const GValue *magnitudes;
          const GValue *phases;
          const GValue *mag, *phase;
          gdouble freq;
          guint i;

          if (!gst_structure_get_clock_time (s, "endtime", &endtime))
            endtime = GST_CLOCK_TIME_NONE;

          g_print ("New spectrum message, endtime %" GST_TIME_FORMAT "\n", GST_TIME_ARGS (endtime));

          magnitudes = gst_structure_get_value (s, "magnitude");
          phases = gst_structure_get_value (s, "phase");

          for (i = 0; i < 7; i++)
            {
              freq = (gdouble) ((AUDIOFREQ / 2) * i + AUDIOFREQ / 4) / spect_bands;
              mag = gst_value_list_get_value (magnitudes, i);
              phase = gst_value_list_get_value (phases, i);

              if (mag != NULL && phase != NULL)
                {
                  g_print ("band %d (freq %g): magnitude %f db phase %f\n", i, freq, g_value_get_float (mag), g_value_get_float (phase));
                }
            }
          g_print ("\n");
        }
    }
  return TRUE;
}

static void
test_test_page_tuner_finalize (GObject *object)
{
  TestTestPageTuner *self = TEST_TEST_PAGE_TUNER (object);

  gst_element_set_state (self->pipeline, GST_STATE_NULL);
  gst_object_unref (self->pipeline);

  G_OBJECT_CLASS (test_test_page_tuner_parent_class)->finalize (object);
}

static void
test_test_page_tuner_class_init (TestTestPageTunerClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = test_test_page_tuner_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/testtest/pages/tuner/test_test-page-tuner.ui");
}

static void
test_test_page_tuner_init (TestTestPageTuner *self)
{
  GstElement *src, *audioconvert, *spectrum, *sink;
  GstBus *bus;
  GstCaps *caps;

  gtk_widget_init_template (GTK_WIDGET (self));

  return;
  self->pipeline = gst_pipeline_new ("pipeline");

  //src = gst_element_factory_make ("pulsesrc", "src");
  src = gst_element_factory_make ("audiotestsrc", "src");
  g_object_set (G_OBJECT (src), "wave", 0, "freq", 400.0, NULL);
  audioconvert = gst_element_factory_make ("audioconvert", NULL);
  g_assert (audioconvert);

  spectrum = gst_element_factory_make ("spectrum", "spectrum");
  g_object_set (G_OBJECT (spectrum), "bands", spect_bands, "threshold", -80, "post-messages", TRUE, "message-phase", TRUE, NULL);

  sink = gst_element_factory_make ("fakesink", "sink");
  g_object_set (G_OBJECT (sink), "sync", TRUE, NULL);

  gst_bin_add_many (GST_BIN (self->pipeline), src, audioconvert, spectrum, sink, NULL);

  caps = gst_caps_new_simple ("audio/x-raw", "rate", G_TYPE_INT, AUDIOFREQ, NULL);

  if (!gst_element_link (src, audioconvert) ||
      !gst_element_link_filtered (audioconvert, spectrum, caps) ||
      !gst_element_link (spectrum, sink))
    {
      g_print ("Can't link elements!\n");
    }
  gst_caps_unref (caps);

  bus = gst_element_get_bus (self->pipeline);
  gst_bus_add_watch (bus, message_handler, NULL);
  gst_object_unref (bus);

  gst_element_set_state (self->pipeline, GST_STATE_PLAYING);
}
