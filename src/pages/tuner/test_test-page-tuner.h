#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define TEST_TEST_TYPE_PAGE_TUNER (test_test_page_tuner_get_type())

G_DECLARE_FINAL_TYPE (TestTestPageTuner, test_test_page_tuner, TEST_TEST, PAGE_TUNER, AdwBin);

G_END_DECLS
