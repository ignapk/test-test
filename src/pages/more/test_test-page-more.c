#include "test_test-page-more.h"

struct _TestTestPageMore
{
  AdwBin parent_instance;
};

G_DEFINE_TYPE (TestTestPageMore, test_test_page_more, ADW_TYPE_BIN)

static void
test_test_page_more_class_init (TestTestPageMoreClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/example/testtest/pages/more/test_test-page-more.ui");
}

static void
test_test_page_more_init (TestTestPageMore *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
