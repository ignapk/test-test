#pragma once

#include <adwaita.h>

G_BEGIN_DECLS

#define TEST_TEST_TYPE_PAGE_MORE (test_test_page_more_get_type())

G_DECLARE_FINAL_TYPE (TestTestPageMore, test_test_page_more, TEST_TEST, PAGE_MORE, AdwBin)

G_END_DECLS
